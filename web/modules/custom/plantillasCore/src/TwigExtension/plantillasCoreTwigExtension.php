<?php
/* Class DefaultService.
 *
 * @package Drupal\plantillasCore
 */
namespace Drupal\plantillasCore\TwigExtension;

class plantillasCoreTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'plantillasCore_twig_extension';
  }

  /**
   * In this function we can declare the extension function
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('getNombreSitio', array($this, 'getNombreSitio')),
	  new \Twig_SimpleFunction('getDireccionSitio', array($this, 'getDireccionSitio')),
      );
  }

  /**
   * The php function to load a given block
   */
  public function getNombreSitio() {
    $config = \Drupal::config('plantillasCore.settings');
    $nombre_sitio = $config->get('nombre_sitio');
    echo $nombre_sitio;
    return $nombre_sitio;
  }

  public function getDireccionSitio() {
  	$config = \Drupal::config('plantillasCore.settings');
  	$direccion_sitio = $config->get('direccion_sitio.value');
  	return $direccion_sitio;
  }

}

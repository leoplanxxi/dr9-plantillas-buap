<?php
 
/**
 
 * @file
 
 * Contains \Drupal\simple\Form\ConfigForm.
 
 */
 
namespace Drupal\plantillasCore\Form;
 
use Drupal\Core\Form\ConfigFormBase;
 
use Drupal\Core\Form\FormStateInterface;
 
class ConfigForm extends ConfigFormBase {
 
  /**
 
   * {@inheritdoc}
 
   */
 
  public function getFormId() {
 
    return 'plantillasCore_config_form';
 
  }
 
  /**
 
   * {@inheritdoc}
 
   */
 
  public function buildForm(array $form, FormStateInterface $form_state) {
 
    $form = parent::buildForm($form, $form_state);
 
    $config = $this->config('plantillasCore.settings');
 
    $form['nombre_sitio'] = array(
      '#type' => 'textfield',
      '#title' => 'Nombre del sitio',
      '#default_value' => $config->get('plantillasCore.nombre_sitio'),
      '#required' => TRUE,
    );
    
    $form['direccion_sitio'] = array(
      '#type' => 'text_format',
      '#title' => 'Dirección',
      '#default_value' => $config->get('plantillasCore.direccion_sitio.value'),
      '#required' => TRUE,
      '#format' => 'full_html',
    );
 
 
    return $form;
 
  }
 
  /**
 
   * {@inheritdoc}
 
   */
 
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('plantillasCore.settings');
    $config->set('plantillasCore.nombre_sitio', $form_state->getValue('nombre_sitio'));
    $config->set('plantillasCore.direccion_sitio', $form_state->getValue('direccion_sitio'));
    $config->save();
 
    return parent::submitForm($form, $form_state);
 
  }
 
  /**
 
   * {@inheritdoc}
 
   */
 
  protected function getEditableConfigNames() {
 
    return [
      'plantillasCore.settings',
    ];
 
  }
 
}

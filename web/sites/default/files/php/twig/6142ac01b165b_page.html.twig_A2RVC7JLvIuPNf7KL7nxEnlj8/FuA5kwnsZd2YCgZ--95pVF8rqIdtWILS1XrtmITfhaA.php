<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/plantilla_buap/templates/page.html.twig */
class __TwigTemplate_45e0489f389691c507e89fe374e7418eeae8eeb6fbc6c6534b91ee6f85f8b548 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 72
        echo "<div id=\"page-wrapper\">
  <div id=\"page\">
    <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        // line 74
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Site header"));
        echo "\">
\t\t<div class=\"col-12 l-header\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-2 mr-auto\">
\t\t\t\t\t<img src=\"http://devserver.buap.mx/dev-drupal/sites/all/themes/plantilla_1/images/escudo_blanco.png\" class=\"img img-fluid\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-5 ml-auto\">
\t\t\t        <nav class=\"navbar navbar-expand-sm navbar-light bg-buap\">
\t\t\t            <div class=\"collapse navbar-collapse navbars\" id=\"collapse_target2\">
\t\t\t                <ul class=\"navbar-nav mx-auto\">
\t\t\t                    <li class=\"nav-item\">
\t\t\t                        <a class=\"nav-link\" href=\"#\">Autoservicios</a>
\t\t\t                    </li>
\t\t\t                    <li class=\"nav-item\">
\t\t\t                        <a class=\"nav-link\" href=\"#\">Correo BUAP</a>
\t\t\t                    </li>  
\t\t\t                </ul>
\t\t\t            </div>
\t\t\t        </nav>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-12 l-menu-secundario\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-3 ml-auto l-name\">
\t\t\t\t\t<h2>Mi sitio BUAP</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        // line 103
        echo "\t\t\t";
        $context["items"] = call_user_func_array($this->env->getFunction('simplify_menu')->getCallable(), ["menu-de-sitio-buap"]);
        // line 104
        echo "\t\t\t
\t\t\t";
        // line 106
        echo "\t\t\t<nav class=\"navbar navbar-expand-sm navbar-light bg-buap-main\">
\t            <div class=\"collapse navbar-collapse navbars\" id=\"collapse_target2\">
                \t<ul class=\"navbar-nav mx-auto\">

\t\t\t\t  ";
        // line 110
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["items"] ?? null), "menu_tree", [], "any", false, false, true, 110));
        foreach ($context['_seq'] as $context["_key"] => $context["menu_item"]) {
            // line 111
            echo "\t\t\t\t    <li class=\"nav-item\">
\t\t\t\t      <a class=\"nav-link\" href=\"";
            // line 112
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["menu_item"], "url", [], "any", false, false, true, 112), 112, $this->source), "html", null, true);
            echo "\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["menu_item"], "text", [], "any", false, false, true, 112), 112, $this->source), "html", null, true);
            echo "</a>
\t\t\t\t    </li>
\t\t\t\t  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "\t\t\t\t  </ul>
\t\t\t\t </div>
\t\t\t</nav>
\t\t</div>
    </header>
 \t
    <div id=\"main-wrapper\" class=\"layout-main-wrapper clearfix\">
      ";
        // line 122
        $this->displayBlock('content', $context, $blocks);
        // line 149
        echo "\t";
        if (($context["is_front"] ?? null)) {
            // line 150
            echo "      <script>
      \tvar xmlhttp = new XMLHttpRequest();
      \txmlhttp.onreadystatechange = function() {
      \t  if (this.readyState == 4 && this.status == 200) {
      \t    var myArr = JSON.parse(this.responseText);
      \t    for(i=0;i<=2;i++) {
      \t    \tnid = myArr[i]['nid'];
      \t    \ttitle = myArr[i]['title'];
      \t    \timg = myArr[i]['filename'];

      \t    \tvar parentDivBoletin = document.getElementById(\"noticias-boletin\").innerHTML += \"<div class='col-4 col-noticia-boletin'><div class='content'><div class='noticias-buap'><div class='imagen'><a href='https://boletin.buap.mx/?q=node/\" + nid + \"' target='_blank'><img src='\" + img + \"' class='img img-fluid' /></a></div><div class='titulo'>\" + title + \"</div><div class='leer-mas'><a href='https://boletin.buap.mx/?q=node/\" + nid + \"' target='_blank'>Leer Más</a></div></div></div></div>\";
      \t    }
      \t    
      \t  }
      \t};
      \txmlhttp.open(\"GET\", \"https://aplicaciones.buap.mx/app-buap-common/index.php/API/getLatestNoticias_p\", true);
      \txmlhttp.send();
      </script>
      <div class=\"container-fluid container-noticias\">
      \t<div class=\"row\">
      \t\t<div class=\"col-12 col-noticias-buap\">
      \t\t\t<h2>Entérate de lo que sucede en la BUAP</h2>
      \t\t\t<div class=\"container\">
      \t\t\t\t<div class=\"row\"  id=\"noticias-boletin\">

      \t\t\t\t</div>
      \t\t\t</div>
      \t\t</div>
      \t</div>
      </div>

     
      ";
        }
        // line 183
        echo "    </div>
    
    <footer class=\"site-footer .l-footer\">
      ";
        // line 186
        $this->displayBlock('footer', $context, $blocks);
        // line 209
        echo "    </footer>
  </div>
</div>
";
    }

    // line 122
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 123
        echo "        <div id=\"main\" class=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null), 123, $this->source), "html", null, true);
        echo "\">
          ";
        // line 124
        if (($context["is_front"] ?? null)) {
            // line 125
            echo "          <style>
          \t.view-lienzo-pagina-destino .node__title, .view-slider-pagina-destino .node__title { display: none; }
          </style>
          ";
            // line 129
            echo "          ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["slider_principal"] ?? null), 129, $this->source), "html", null, true);
            echo "
          ";
            // line 130
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["recursos"] ?? null), 130, $this->source), "html", null, true);
            echo "
          <p>&nbsp;</p>
          ";
        } else {
            // line 133
            echo "          \t";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 133), 133, $this->source), "html", null, true);
            echo "
              <div class=\"row row-offcanvas row-offcanvas-left clearfix\">
                  <main";
            // line 135
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_attributes"] ?? null), 135, $this->source), "html", null, true);
            echo ">
                    <section class=\"section\">
                      <a id=\"main-content\" tabindex=\"-1\"></a>
                      ";
            // line 138
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 138), 138, $this->source), "html", null, true);
            echo "
                    </section>
                  </main>
                
              </div>
                          
          ";
        }
        // line 145
        echo "          
            
        </div>
      ";
    }

    // line 186
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 187
        echo "        <div class=\"col-12 row\">
        \t<div class=\"col-2\">
        \t\t<img src=\"http://devserver.buap.mx/dev-drupal/sites/all/themes/plantilla_1/images/logo_buap.png\" class=\"img img-fluid\">
        \t</div>
        \t<div class=\"col-3\">
\t        \t<strong>Benemérita Universidad Autónoma de Puebla</strong>
\t        \t<p>4 Sur 104 Centro Histórico C.P. 72000<br>
\t        \tTeléfono +52 (222) 229 55 00</p>
        \t</div>
        \t<div class=\"col-3 col-links-institucionales\">
        \t\t<a href=\"http://www.transparencia.buap.mx/\">Transparencia y Acceso a la Información</a><br>
        \t\t<a href=\"https://consultapublicamx.inai.org.mx/vut-web/?idSujetoObigadoParametro=4479&idEntidadParametro=21&idSectorParametro=24\">Obligaciones de Transparencia</a><br>
        \t\t<a href=\"http://www.pdi.buap.mx/\">PDI 2017-2021</a><br>
        \t\t<a href=\"http://www.buap.mx/privacidad\">Aviso de Privacidad</a>
        \t</div>
        \t<div class=\"col-4\">
        \t\t";
        // line 205
        echo " 
        \t</div>
        </div>
      ";
    }

    public function getTemplateName()
    {
        return "themes/custom/plantilla_buap/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 205,  229 => 187,  225 => 186,  218 => 145,  208 => 138,  202 => 135,  196 => 133,  190 => 130,  185 => 129,  180 => 125,  178 => 124,  173 => 123,  169 => 122,  162 => 209,  160 => 186,  155 => 183,  120 => 150,  117 => 149,  115 => 122,  106 => 115,  95 => 112,  92 => 111,  88 => 110,  82 => 106,  79 => 104,  76 => 103,  45 => 74,  41 => 72,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bootstrap Barrio's theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template normally located in the
 * core/modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 * - logo: The url of the logo image, as defined in theme settings.
 * - site_name: The name of the site. This is empty when displaying the site
 *   name has been disabled in the theme settings.
 * - site_slogan: The slogan of the site. This is empty when displaying the site
 *   slogan has been disabled in theme settings.

 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.top_header: Items for the top header region.
 * - page.top_header_form: Items for the top header form region.
 * - page.header: Items for the header region.
 * - page.header_form: Items for the header form region.
 * - page.highlighted: Items for the highlighted region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.featured_top: Items for the featured top region.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.featured_bottom_first: Items for the first featured bottom region.
 * - page.featured_bottom_second: Items for the second featured bottom region.
 * - page.featured_bottom_third: Items for the third featured bottom region.
 * - page.footer_first: Items for the first footer column.
 * - page.footer_second: Items for the second footer column.
 * - page.footer_third: Items for the third footer column.
 * - page.footer_fourth: Items for the fourth footer column.
 * - page.footer_fifth: Items for the fifth footer column.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * Theme variables:
 * - navbar_top_attributes: Items for the header region.
 * - navbar_attributes: Items for the header region.
 * - content_attributes: Items for the header region.
 * - sidebar_first_attributes: Items for the highlighted region.
 * - sidebar_second_attributes: Items for the primary menu region.
 * - sidebar_collapse: If the sidebar_first will collapse.
 *
 * @see template_preprocess_page()
 * @see bootstrap_barrio_preprocess_page()
 * @see html.html.twig
 */

 https://www.codeply.com/go/PvHpcBNuAp
#}
<div id=\"page-wrapper\">
  <div id=\"page\">
    <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"{{ 'Site header'|t}}\">
\t\t<div class=\"col-12 l-header\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-2 mr-auto\">
\t\t\t\t\t<img src=\"http://devserver.buap.mx/dev-drupal/sites/all/themes/plantilla_1/images/escudo_blanco.png\" class=\"img img-fluid\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-5 ml-auto\">
\t\t\t        <nav class=\"navbar navbar-expand-sm navbar-light bg-buap\">
\t\t\t            <div class=\"collapse navbar-collapse navbars\" id=\"collapse_target2\">
\t\t\t                <ul class=\"navbar-nav mx-auto\">
\t\t\t                    <li class=\"nav-item\">
\t\t\t                        <a class=\"nav-link\" href=\"#\">Autoservicios</a>
\t\t\t                    </li>
\t\t\t                    <li class=\"nav-item\">
\t\t\t                        <a class=\"nav-link\" href=\"#\">Correo BUAP</a>
\t\t\t                    </li>  
\t\t\t                </ul>
\t\t\t            </div>
\t\t\t        </nav>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-12 l-menu-secundario\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-3 ml-auto l-name\">
\t\t\t\t\t<h2>Mi sitio BUAP</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t{# Get menu items #}
\t\t\t{% set items = simplify_menu('menu-de-sitio-buap') %}
\t\t\t
\t\t\t{# Iterate menu tree #}
\t\t\t<nav class=\"navbar navbar-expand-sm navbar-light bg-buap-main\">
\t            <div class=\"collapse navbar-collapse navbars\" id=\"collapse_target2\">
                \t<ul class=\"navbar-nav mx-auto\">

\t\t\t\t  {% for menu_item in items.menu_tree %}
\t\t\t\t    <li class=\"nav-item\">
\t\t\t\t      <a class=\"nav-link\" href=\"{{ menu_item.url }}\">{{ menu_item.text }}</a>
\t\t\t\t    </li>
\t\t\t\t  {% endfor %}
\t\t\t\t  </ul>
\t\t\t\t </div>
\t\t\t</nav>
\t\t</div>
    </header>
 \t
    <div id=\"main-wrapper\" class=\"layout-main-wrapper clearfix\">
      {% block content %}
        <div id=\"main\" class=\"{{ container }}\">
          {% if is_front %}
          <style>
          \t.view-lienzo-pagina-destino .node__title, .view-slider-pagina-destino .node__title { display: none; }
          </style>
          {# <div class=\"col-12\" style=\"height: 360px; border: 1px solid black; text-align:center;\"> SLIDER PRINCIPAL </div> #}
          {{ slider_principal }}
          {{ recursos }}
          <p>&nbsp;</p>
          {% else %}
          \t{{ page.breadcrumb }}
              <div class=\"row row-offcanvas row-offcanvas-left clearfix\">
                  <main{{ content_attributes }}>
                    <section class=\"section\">
                      <a id=\"main-content\" tabindex=\"-1\"></a>
                      {{ page.content }}
                    </section>
                  </main>
                
              </div>
                          
          {% endif %}
          
            
        </div>
      {% endblock %}
\t{% if is_front %}
      <script>
      \tvar xmlhttp = new XMLHttpRequest();
      \txmlhttp.onreadystatechange = function() {
      \t  if (this.readyState == 4 && this.status == 200) {
      \t    var myArr = JSON.parse(this.responseText);
      \t    for(i=0;i<=2;i++) {
      \t    \tnid = myArr[i]['nid'];
      \t    \ttitle = myArr[i]['title'];
      \t    \timg = myArr[i]['filename'];

      \t    \tvar parentDivBoletin = document.getElementById(\"noticias-boletin\").innerHTML += \"<div class='col-4 col-noticia-boletin'><div class='content'><div class='noticias-buap'><div class='imagen'><a href='https://boletin.buap.mx/?q=node/\" + nid + \"' target='_blank'><img src='\" + img + \"' class='img img-fluid' /></a></div><div class='titulo'>\" + title + \"</div><div class='leer-mas'><a href='https://boletin.buap.mx/?q=node/\" + nid + \"' target='_blank'>Leer Más</a></div></div></div></div>\";
      \t    }
      \t    
      \t  }
      \t};
      \txmlhttp.open(\"GET\", \"https://aplicaciones.buap.mx/app-buap-common/index.php/API/getLatestNoticias_p\", true);
      \txmlhttp.send();
      </script>
      <div class=\"container-fluid container-noticias\">
      \t<div class=\"row\">
      \t\t<div class=\"col-12 col-noticias-buap\">
      \t\t\t<h2>Entérate de lo que sucede en la BUAP</h2>
      \t\t\t<div class=\"container\">
      \t\t\t\t<div class=\"row\"  id=\"noticias-boletin\">

      \t\t\t\t</div>
      \t\t\t</div>
      \t\t</div>
      \t</div>
      </div>

     
      {% endif %}
    </div>
    
    <footer class=\"site-footer .l-footer\">
      {% block footer %}
        <div class=\"col-12 row\">
        \t<div class=\"col-2\">
        \t\t<img src=\"http://devserver.buap.mx/dev-drupal/sites/all/themes/plantilla_1/images/logo_buap.png\" class=\"img img-fluid\">
        \t</div>
        \t<div class=\"col-3\">
\t        \t<strong>Benemérita Universidad Autónoma de Puebla</strong>
\t        \t<p>4 Sur 104 Centro Histórico C.P. 72000<br>
\t        \tTeléfono +52 (222) 229 55 00</p>
        \t</div>
        \t<div class=\"col-3 col-links-institucionales\">
        \t\t<a href=\"http://www.transparencia.buap.mx/\">Transparencia y Acceso a la Información</a><br>
        \t\t<a href=\"https://consultapublicamx.inai.org.mx/vut-web/?idSujetoObigadoParametro=4479&idEntidadParametro=21&idSectorParametro=24\">Obligaciones de Transparencia</a><br>
        \t\t<a href=\"http://www.pdi.buap.mx/\">PDI 2017-2021</a><br>
        \t\t<a href=\"http://www.buap.mx/privacidad\">Aviso de Privacidad</a>
        \t</div>
        \t<div class=\"col-4\">
        \t\t{#<strong>{{ getNombreSitio }}</strong>
        \t\t{{ getDireccionSitio }}
        \t\t{{ dump(getNombreSitio()) }}#} 
        \t</div>
        </div>
      {% endblock %}
    </footer>
  </div>
</div>
", "themes/custom/plantilla_buap/templates/page.html.twig", "/var/www/html/devserver/dr9-plantillas-buap/web/themes/custom/plantilla_buap/templates/page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 103, "for" => 110, "block" => 122, "if" => 149);
        static $filters = array("t" => 74, "escape" => 112);
        static $functions = array("simplify_menu" => 103);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'for', 'block', 'if'],
                ['t', 'escape'],
                ['simplify_menu']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

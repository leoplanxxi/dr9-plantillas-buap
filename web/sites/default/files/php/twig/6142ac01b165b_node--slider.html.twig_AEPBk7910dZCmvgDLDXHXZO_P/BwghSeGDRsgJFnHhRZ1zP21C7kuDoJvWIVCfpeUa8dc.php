<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/plantilla_buap/templates/recursos/node--slider.html.twig */
class __TwigTemplate_77f038aee74e420cf12d25a1cd98451076f5d2e2b816a8ad4003d88f7cadf776 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'submitted' => [$this, 'block_submitted'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("bootstrap_barrio/node"), "html", null, true);
        echo "
";
        // line 63
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("core/jquery"), "html", null, true);
        echo "

";
        // line 66
        $context["classes"] = [0 => "node", 1 => ("node--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 68
($context["node"] ?? null), "bundle", [], "any", false, false, true, 68), 68, $this->source))), 2 => ((twig_get_attribute($this->env, $this->source,         // line 69
($context["node"] ?? null), "isPromoted", [], "method", false, false, true, 69)) ? ("node--promoted") : ("")), 3 => ((twig_get_attribute($this->env, $this->source,         // line 70
($context["node"] ?? null), "isSticky", [], "method", false, false, true, 70)) ? ("node--sticky") : ("")), 4 => (( !twig_get_attribute($this->env, $this->source,         // line 71
($context["node"] ?? null), "isPublished", [], "method", false, false, true, 71)) ? ("node--unpublished") : ("")), 5 => ((        // line 72
($context["view_mode"] ?? null)) ? (("node--view-mode-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null), 72, $this->source)))) : ("")), 6 => "clearfix"];
        // line 76
        echo "
";
        // line 77
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 77), 0, [], "any", false, false, true, 77), "value", [], "any", false, false, true, 77) == "Sencillo")) {
            // line 78
            echo "\t";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 78), 0, [], "any", false, false, true, 78), "value", [], "any", false, false, true, 78) == 1)) {
                // line 79
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 79, $this->source), [0 => "col-md-12", 1 => "slider-1-cols"]);
                // line 80
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 80), 0, [], "any", false, false, true, 80), "value", [], "any", false, false, true, 80) == 2)) {
                // line 81
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 81, $this->source), [0 => "col-md-6", 1 => "slider-2-cols"]);
                // line 82
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 82), 0, [], "any", false, false, true, 82), "value", [], "any", false, false, true, 82) == 3)) {
                // line 83
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 83, $this->source), [0 => "col-md-4", 1 => "slider-3-cols"]);
                // line 84
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 84), 0, [], "any", false, false, true, 84), "value", [], "any", false, false, true, 84) == 4)) {
                // line 85
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 85, $this->source), [0 => "col-md-3", 1 => "slider-4-cols"]);
                // line 86
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 86), 0, [], "any", false, false, true, 86), "value", [], "any", false, false, true, 86) == "Slider Principal")) {
                // line 87
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 87, $this->source), [0 => "col-md-12", 1 => "slider-principal"]);
                // line 88
                echo "\t";
            } else {
                // line 89
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 89, $this->source), [0 => "col-md-12", 1 => "slider-1-cols"]);
                // line 90
                echo "\t";
            }
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 91
($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 91), 0, [], "any", false, false, true, 91), "value", [], "any", false, false, true, 91) == "Avanzado")) {
            // line 92
            echo "\t";
            $context["bsclasses"] = $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_personalizado_", [], "any", false, false, true, 92), 0, [], "any", false, false, true, 92), "value", [], "any", false, false, true, 92), 92, $this->source);
            echo " 
\t";
            // line 93
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 93, $this->source), [0 => ($context["bsclasses"] ?? null)]);
            // line 94
            echo "\t";
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 94, $this->source), [0 => "slider-p-cols"]);
        }
        // line 96
        echo "
";
        // line 97
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 97), 0, [], "any", false, false, true, 97), "value", [], "any", false, false, true, 97) == "Sencillo")) {
            // line 98
            echo "\t<article";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 98), 98, $this->source), "html", null, true);
            echo ">
";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 99
($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 99), 0, [], "any", false, false, true, 99), "value", [], "any", false, false, true, 99) == "Avanzado")) {
            // line 100
            echo "\t";
            $context["height_user"] = $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_altura_columna_px_", [], "any", false, false, true, 100), 0, [], "any", false, false, true, 100), "value", [], "any", false, false, true, 100), 100, $this->source);
            // line 101
            echo "\t
\t<article";
            // line 102
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 102), 102, $this->source), "html", null, true);
            echo " style=\"height: ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["height_user"] ?? null), 102, $this->source), "html", null, true);
            echo "px; overflow: hidden; \">
";
        }
        // line 104
        echo "
  <header>
    ";
        // line 106
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 106, $this->source), "html", null, true);
        echo "
    ";
        // line 107
        if ((($context["label"] ?? null) &&  !($context["page"] ?? null))) {
            // line 108
            echo "      <h2";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => "node__title"], "method", false, false, true, 108), 108, $this->source), "html", null, true);
            echo ">
        <a href=\"";
            // line 109
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 109, $this->source), "html", null, true);
            echo "\" rel=\"bookmark\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 109, $this->source), "html", null, true);
            echo "</a>
      </h2>
    ";
        }
        // line 112
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 112, $this->source), "html", null, true);
        echo "
    ";
        // line 113
        if (($context["display_submitted"] ?? null)) {
            // line 114
            echo "      <div class=\"node__meta\">
        ";
            // line 115
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["author_picture"] ?? null), 115, $this->source), "html", null, true);
            echo "
        ";
            // line 116
            $this->displayBlock('submitted', $context, $blocks);
            // line 121
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["metadata"] ?? null), 121, $this->source), "html", null, true);
            echo "
      </div>
    ";
        }
        // line 124
        echo "  </header>
 
  <div";
        // line 126
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => "node__content", 1 => "clearfix", 2 => "carousel", 3 => "slide"], "method", false, false, true, 126), 126, $this->source), "html", null, true);
        echo " ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "setAttribute", [0 => "id", 1 => ("slider-" . $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "id", [], "any", false, false, true, 126), 126, $this->source))], "method", false, false, true, 126), 126, $this->source), "html", null, true);
        echo " data-ride=\"carousel\">
  \t<div class=\"carousel-inner\">
\t    ";
        // line 128
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_elementos_slider", [], "any", false, false, true, 128));
        foreach ($context['_seq'] as $context["key"] => $context["item"]) {
            if ((twig_first($this->env, $context["key"]) != "#")) {
                // line 129
                echo "\t     ";
                // line 130
                echo "\t     ";
                $context["img_uri"] = Drupal\twig_tweak\TwigTweakExtension::fileUriFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["item"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["#paragraph"] ?? null) : null), "field_imagen_elemento_slider", [], "any", false, false, true, 130), 130, $this->source));
                // line 131
                echo "\t     ";
                // line 132
                echo "\t     ";
                if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "#paragraph", [], "array", false, true, true, 132), "field_link_elemento_slider", [], "any", false, true, true, 132), 0, [], "any", true, true, true, 132)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "#paragraph", [], "array", false, true, true, 132), "field_link_elemento_slider", [], "any", false, true, true, 132), 0, [], "any", false, false, true, 132), null)) : (null))) {
                    // line 133
                    echo "\t     \t";
                    $context["has_link"] = true;
                    // line 134
                    echo "\t     ";
                } else {
                    // line 135
                    echo "\t     \t";
                    $context["has_link"] = false;
                    // line 136
                    echo "     \t ";
                }
                // line 137
                echo "\t     ";
                if (($context["key"] == 0)) {
                    // line 138
                    echo "   \t     <div class=\"carousel-item active\">
\t     ";
                } else {
                    // line 140
                    echo "   \t     <div class=\"carousel-item\">
\t     ";
                }
                // line 142
                echo "\t     ";
                if ((($context["has_link"] ?? null) == true)) {
                    // line 143
                    echo "     \t\t";
                    $context["img_link"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["item"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["#paragraph"] ?? null) : null), "field_link_elemento_slider", [], "any", false, false, true, 143), 0, [], "any", false, false, true, 143), "url", [], "any", false, false, true, 143);
                    // line 144
                    echo "     \t\t";
                    $context["img_link_target"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["item"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["#paragraph"] ?? null) : null), "field_link_elemento_slider", [], "any", false, false, true, 144), 0, [], "any", false, false, true, 144), "options", [], "any", false, false, true, 144), "attributes", [], "any", false, false, true, 144), "target", [], "any", false, false, true, 144);
                    // line 145
                    echo "     
     \t\t";
                    // line 146
                    if ((($context["img_link_target"] ?? null) == "_blank")) {
                        // line 147
                        echo "     \t\t\t";
                        $context["img_link_target"] = "target='_blank'";
                        // line 148
                        echo "     \t\t";
                    } else {
                        // line 149
                        echo "     \t\t\t";
                        $context["img_link_target"] = "";
                        // line 150
                        echo "     \t\t";
                    }
                    // line 151
                    echo "     \t\t<a href=\"";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["img_link"] ?? null), 151, $this->source), "html", null, true);
                    echo "\" ";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->sandbox->ensureToStringAllowed(($context["img_link_target"] ?? null), 151, $this->source));
                    echo ">
     \t";
                }
                // line 153
                echo "     \t";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["item"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["#paragraph"] ?? null) : null), "field_mostrar_elemento_slider", [], "any", false, false, true, 153), 0, [], "any", false, false, true, 153), "value", [], "any", false, false, true, 153) == "Imagen")) {
                    // line 154
                    echo "\t     \t";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 154), 0, [], "any", false, false, true, 154), "value", [], "any", false, false, true, 154) == "Sencillo")) {
                        // line 155
                        echo "\t\t\t    ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 155), 0, [], "any", false, false, true, 155), "value", [], "any", false, false, true, 155) == 1)) {
                            echo "\t
\t\t\t    \t<img src=\"";
                            // line 156
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 156, $this->source), "slider_principal_wide"), "html", null, true);
                            echo "\" class=\"imagen-slider\">
\t\t\t    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 157
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 157), 0, [], "any", false, false, true, 157), "value", [], "any", false, false, true, 157) == 2)) {
                            // line 158
                            echo "\t\t\t\t\t<img src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 158, $this->source), "2_columnas_6_cols_bootstrap_"), "html", null, true);
                            echo "\" class=\"imagen-slider\">
\t\t\t    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 159
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 159), 0, [], "any", false, false, true, 159), "value", [], "any", false, false, true, 159) == 3)) {
                            // line 160
                            echo "\t\t\t\t\t<img src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 160, $this->source), "3_columnas_4_cols_bootstrap_"), "html", null, true);
                            echo "\" class=\"imagen-slider\">
\t\t\t    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 161
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 161), 0, [], "any", false, false, true, 161), "value", [], "any", false, false, true, 161) == 4)) {
                            // line 162
                            echo "\t\t\t\t\t<img src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 162, $this->source), "4_columnas_3_cols_bootstrap_"), "html", null, true);
                            echo "\" class=\"imagen-slider\">
\t\t\t\t";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 163
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 163), 0, [], "any", false, false, true, 163), "value", [], "any", false, false, true, 163) == "Slider Principal")) {
                            // line 164
                            echo "\t\t\t     \t<img src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 164, $this->source), "slider_principal_wide"), "html", null, true);
                            echo "\" class=\"imagen-slider\">
\t\t\t\t";
                        }
                        // line 166
                        echo "\t\t\t";
                    } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 166), 0, [], "any", false, false, true, 166), "value", [], "any", false, false, true, 166) == "Avanzado")) {
                        // line 167
                        echo "\t\t\t\t<img src=\"";
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 167, $this->source)]), "html", null, true);
                        echo "\">
\t\t    ";
                    }
                    // line 169
                    echo "\t\t ";
                } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 = $context["item"]) && is_array($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4) || $__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4 instanceof ArrayAccess ? ($__internal_d7fc55f1a54b629533d60b43063289db62e68921ee7a5f8de562bd9d4a2b7ad4["#paragraph"] ?? null) : null), "field_mostrar_elemento_slider", [], "any", false, false, true, 169), 0, [], "any", false, false, true, 169), "value", [], "any", false, false, true, 169) == "Video")) {
                    // line 170
                    echo "\t\t  \t";
                    $context["video_uri"] = call_user_func_array($this->env->getFunction('file_url')->getCallable(), [Drupal\twig_tweak\TwigTweakExtension::fileUriFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, (($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 = $context["item"]) && is_array($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666) || $__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666 instanceof ArrayAccess ? ($__internal_01476f8db28655ee4ee02ea2d17dd5a92599be76304f08cd8bc0e05aced30666["#paragraph"] ?? null) : null), "field_video_elemento_slider", [], "any", false, false, true, 170), 170, $this->source))]);
                    // line 171
                    echo "\t\t \t";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 171), 0, [], "any", false, false, true, 171), "value", [], "any", false, false, true, 171) == "Sencillo")) {
                        // line 172
                        echo "\t\t\t    ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 172), 0, [], "any", false, false, true, 172), "value", [], "any", false, false, true, 172) == 1)) {
                            echo "\t
\t\t\t    \t<video src=\"";
                            // line 173
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["video_uri"] ?? null), 173, $this->source), "html", null, true);
                            echo "\" style=\"width: 1110px; height: 393px;\" loop muted autoplay class='video-slider'>
\t\t\t    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 174
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 174), 0, [], "any", false, false, true, 174), "value", [], "any", false, false, true, 174) == 2)) {
                            // line 175
                            echo "\t\t\t\t\t<video src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["video_uri"] ?? null), 175, $this->source), "html", null, true);
                            echo "\" style=\"width: 540px; height: 322px;\" loop muted autoplay class='video-slider'>
\t\t\t    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 176
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 176), 0, [], "any", false, false, true, 176), "value", [], "any", false, false, true, 176) == 3)) {
                            // line 177
                            echo "\t\t\t\t\t<video src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["video_uri"] ?? null), 177, $this->source), "html", null, true);
                            echo "\" style=\"width: 350px; height: 209px;\" loop muted autoplay class='video-slider'>
\t\t\t    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 178
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 178), 0, [], "any", false, false, true, 178), "value", [], "any", false, false, true, 178) == 4)) {
                            // line 179
                            echo "\t\t\t\t\t<video src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["video_uri"] ?? null), 179, $this->source), "html", null, true);
                            echo "\" style=\"width: 255px; height: 205px;\" loop muted autoplay class='video-slider'>
\t\t\t\t";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 180
($context["node"] ?? null), "field_columnas_slider", [], "any", false, false, true, 180), 0, [], "any", false, false, true, 180), "value", [], "any", false, false, true, 180) == "Slider Principal")) {
                            // line 181
                            echo "\t\t\t\t\t<video src=\"";
                            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["video_uri"] ?? null), 181, $this->source), "html", null, true);
                            echo "\" style=\"width: 1110px; height: 393px;\" loop muted autoplay class='video-slider'>
\t\t\t\t";
                        }
                        // line 183
                        echo "\t\t\t";
                    } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 183), 0, [], "any", false, false, true, 183), "value", [], "any", false, false, true, 183) == "Avanzado")) {
                        // line 184
                        echo "\t\t\t\t\t<video src=\"";
                        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["video_uri"] ?? null), 184, $this->source), "html", null, true);
                        echo "\" loop muted autoplay class='video-slider'>
\t\t    ";
                    }
                    // line 186
                    echo "\t\t ";
                } else {
                    // line 187
                    echo "\t\t Invalido
\t\t ";
                }
                // line 189
                echo "\t     \t";
                if ((($context["has_link"] ?? null) == true)) {
                    // line 190
                    echo "     \t   \t\t</a>
     \t   \t";
                }
                // line 191
                echo " 
\t     </div>
   \t    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 194
        echo "    </div>
     <a class=\"carousel-control-prev\" role=\"button\" data-slide=\"prev\" href=";
        // line 195
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ("#slider-" . $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "id", [], "any", false, false, true, 195), 195, $this->source)), "html", null, true);
        echo ">
    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Previous</span>
  </a>
  <a class=\"carousel-control-next\" role=\"button\" data-slide=\"next\"  href=";
        // line 199
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ("#slider-" . $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "id", [], "any", false, false, true, 199), 199, $this->source)), "html", null, true);
        echo ">
    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Next</span>
  </a>
  </div>
</article>

";
    }

    // line 116
    public function block_submitted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 117
        echo "          <em";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["author_attributes"] ?? null), 117, $this->source), "html", null, true);
        echo ">
            ";
        // line 118
        echo t("Submitted by @author_name on @date", array("@author_name" => ($context["author_name"] ?? null), "@date" => ($context["date"] ?? null), ));
        // line 119
        echo "          </em>
        ";
    }

    public function getTemplateName()
    {
        return "themes/custom/plantilla_buap/templates/recursos/node--slider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  432 => 119,  430 => 118,  425 => 117,  421 => 116,  409 => 199,  402 => 195,  399 => 194,  390 => 191,  386 => 190,  383 => 189,  379 => 187,  376 => 186,  370 => 184,  367 => 183,  361 => 181,  359 => 180,  354 => 179,  352 => 178,  347 => 177,  345 => 176,  340 => 175,  338 => 174,  334 => 173,  329 => 172,  326 => 171,  323 => 170,  320 => 169,  314 => 167,  311 => 166,  305 => 164,  303 => 163,  298 => 162,  296 => 161,  291 => 160,  289 => 159,  284 => 158,  282 => 157,  278 => 156,  273 => 155,  270 => 154,  267 => 153,  259 => 151,  256 => 150,  253 => 149,  250 => 148,  247 => 147,  245 => 146,  242 => 145,  239 => 144,  236 => 143,  233 => 142,  229 => 140,  225 => 138,  222 => 137,  219 => 136,  216 => 135,  213 => 134,  210 => 133,  207 => 132,  205 => 131,  202 => 130,  200 => 129,  195 => 128,  188 => 126,  184 => 124,  177 => 121,  175 => 116,  171 => 115,  168 => 114,  166 => 113,  161 => 112,  153 => 109,  148 => 108,  146 => 107,  142 => 106,  138 => 104,  131 => 102,  128 => 101,  125 => 100,  123 => 99,  118 => 98,  116 => 97,  113 => 96,  109 => 94,  107 => 93,  102 => 92,  100 => 91,  97 => 90,  94 => 89,  91 => 88,  88 => 87,  85 => 86,  82 => 85,  79 => 84,  76 => 83,  73 => 82,  70 => 81,  67 => 80,  64 => 79,  61 => 78,  59 => 77,  56 => 76,  54 => 72,  53 => 71,  52 => 70,  51 => 69,  50 => 68,  49 => 66,  44 => 63,  40 => 62,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bootstrap Barrio's theme implementation to display a node.
 *
 * Available variables:
 * - node: The node entity with limited access to object properties and methods.
     Only \"getter\" methods (method names starting with \"get\", \"has\", or \"is\")
     and a few common methods such as \"id\" and \"label\" are available. Calling
     other methods (such as node.delete) will result in an exception.
 * - label: The title of the node.
 * - content: All node items. Use {{ content }} to print them all,
 *   or print a subset such as {{ content.field_example }}. Use
 *   {{ content|without('field_example') }} to temporarily suppress the printing
 *   of a given child element.
 * - author_picture: The node author user entity, rendered using the \"compact\"
 *   view mode.
 * - metadata: Metadata for this node.
 * - date: Themed creation date field.
 * - author_name: Themed author name field.
 * - url: Direct URL of the current node.
 * - display_submitted: Whether submission information should be displayed.
 * - attributes: HTML attributes for the containing element.
 *   The attributes.class element may contain one or more of the following
 *   classes:
 *   - node: The current template type (also known as a \"theming hook\").
 *   - node--type-[type]: The current node type. For example, if the node is an
 *     \"Article\" it would result in \"node--type-article\". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node--view-mode-[view_mode]: The View Mode of the node; for example, a
 *     teaser would result in: \"node--view-mode-teaser\", and
 *     full: \"node--view-mode-full\".
 *   The following are controlled through the node publishing options.
 *   - node--promoted: Appears on nodes promoted to the front page.
 *   - node--sticky: Appears on nodes ordered above other non-sticky nodes in
 *     teaser listings.
 *   - node--unpublished: Appears on unpublished nodes visible only to site
 *     admins.
 * - title_attributes: Same as attributes, except applied to the main title
 *   tag that appears in the template.
 * - content_attributes: Same as attributes, except applied to the main
 *   content tag that appears in the template.
 * - author_attributes: Same as attributes, except applied to the author of
 *   the node tag that appears in the template.
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - view_mode: View mode; for example, \"teaser\" or \"full\".
 * - teaser: Flag for the teaser state. Will be true if view_mode is 'teaser'.
 * - page: Flag for the full page state. Will be true if view_mode is 'full'.
 * - readmore: Flag for more state. Will be true if the teaser content of the
 *   node cannot hold the main body content.
 * - logged_in: Flag for authenticated user status. Will be true when the
 *   current user is a logged-in member.
 * - is_admin: Flag for admin user status. Will be true when the current user
 *   is an administrator.
 *
 * @see template_preprocess_node()
 */
#}
{{ attach_library('bootstrap_barrio/node') }}
{{ attach_library('core/jquery') }}

{%
  set classes = [
    'node',
    'node--type-' ~ node.bundle|clean_class,
    node.isPromoted() ? 'node--promoted',
    node.isSticky() ? 'node--sticky',
    not node.isPublished() ? 'node--unpublished',
    view_mode ? 'node--view-mode-' ~ view_mode|clean_class,
    'clearfix',
  ]
%}

{% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t{% if node.field_columnas_slider.0.value == 1 %}
\t\t{% set classes = classes|merge(['col-md-12', 'slider-1-cols']) %}
\t{% elseif node.field_columnas_slider.0.value == 2 %}
\t\t{% set classes = classes|merge(['col-md-6', 'slider-2-cols']) %}
\t{% elseif node.field_columnas_slider.0.value == 3 %}
\t\t{% set classes = classes|merge(['col-md-4', 'slider-3-cols']) %}
\t{% elseif node.field_columnas_slider.0.value == 4 %}
\t\t{% set classes = classes|merge(['col-md-3', 'slider-4-cols']) %}
\t{% elseif node.field_columnas_slider.0.value == 'Slider Principal' %}
\t\t{% set classes = classes|merge(['col-md-12', 'slider-principal']) %}
\t{% else %}
\t\t{% set classes = classes|merge(['col-md-12', 'slider-1-cols']) %}
\t{% endif %}
{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t{% set bsclasses = node.field_columnas_personalizado_.0.value|raw %} 
\t{% set classes = classes|merge([bsclasses]) %}
\t{% set classes = classes|merge(['slider-p-cols']) %}
{% endif %}

{% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t<article{{ attributes.addClass(classes) }}>
{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t{% set height_user = node.field_altura_columna_px_.0.value|raw %}
\t
\t<article{{ attributes.addClass(classes) }} style=\"height: {{ height_user }}px; overflow: hidden; \">
{% endif %}

  <header>
    {{ title_prefix }}
    {% if label and not page %}
      <h2{{ title_attributes.addClass('node__title') }}>
        <a href=\"{{ url }}\" rel=\"bookmark\">{{ label }}</a>
      </h2>
    {% endif %}
    {{ title_suffix }}
    {% if display_submitted %}
      <div class=\"node__meta\">
        {{ author_picture }}
        {% block submitted %}
          <em{{ author_attributes }}>
            {% trans %}Submitted by {{ author_name }} on {{ date }}{% endtrans %}
          </em>
        {% endblock %}
        {{ metadata }}
      </div>
    {% endif %}
  </header>
 
  <div{{ content_attributes.addClass('node__content', 'clearfix', 'carousel', 'slide') }} {{ content_attributes.setAttribute('id','slider-' ~ node.id) }} data-ride=\"carousel\">
  \t<div class=\"carousel-inner\">
\t    {% for key, item in content.field_elementos_slider if key|first != '#' %}
\t     {# <div class=\"item-{{ key + 1 }}\">{{ item }}</div> #}
\t     {% set img_uri = item[\"#paragraph\"].field_imagen_elemento_slider|file_uri %}
\t     {# if item[\"#paragraph\"].field_link_elemento_slider is not empty and item[\"#paragraph\"].field_link_elemento_slider.0.url != \"\" and item[\"#paragraph\"].field_link_elemento_slider.0.url is not null and item[\"#paragraph\"].field_link_elemento_slider.0 is defined #}
\t     {% if item[\"#paragraph\"].field_link_elemento_slider.0|default(null) %}
\t     \t{% set has_link = true %}
\t     {% else %}
\t     \t{% set has_link = false %}
     \t {% endif %}
\t     {% if key == 0 %}
   \t     <div class=\"carousel-item active\">
\t     {% else %}
   \t     <div class=\"carousel-item\">
\t     {% endif %}
\t     {% if has_link == true %}
     \t\t{% set img_link = item[\"#paragraph\"].field_link_elemento_slider.0.url %}
     \t\t{% set img_link_target = item[\"#paragraph\"].field_link_elemento_slider.0.options.attributes.target %}
     
     \t\t{% if img_link_target == \"_blank\" %}
     \t\t\t{% set img_link_target = \"target='_blank'\" %}
     \t\t{% else %}
     \t\t\t{% set img_link_target = \"\" %}
     \t\t{% endif %}
     \t\t<a href=\"{{ img_link }}\" {{ img_link_target|raw }}>
     \t{% endif %}
     \t{% if item[\"#paragraph\"].field_mostrar_elemento_slider.0.value == \"Imagen\" %}
\t     \t{% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t\t\t    {% if node.field_columnas_imagen.0.value == 1 %}\t
\t\t\t    \t<img src=\"{{ img_uri | image_style('slider_principal_wide') }}\" class=\"imagen-slider\">
\t\t\t    {% elseif node.field_columnas_slider.0.value == 2 %}
\t\t\t\t\t<img src=\"{{ img_uri | image_style('2_columnas_6_cols_bootstrap_') }}\" class=\"imagen-slider\">
\t\t\t    {% elseif node.field_columnas_slider.0.value == 3 %}
\t\t\t\t\t<img src=\"{{ img_uri | image_style('3_columnas_4_cols_bootstrap_') }}\" class=\"imagen-slider\">
\t\t\t    {% elseif node.field_columnas_slider.0.value == 4 %}
\t\t\t\t\t<img src=\"{{ img_uri | image_style('4_columnas_3_cols_bootstrap_') }}\" class=\"imagen-slider\">
\t\t\t\t{% elseif node.field_columnas_slider.0.value == \"Slider Principal\" %}
\t\t\t     \t<img src=\"{{ img_uri | image_style('slider_principal_wide') }}\" class=\"imagen-slider\">
\t\t\t\t{% endif %}
\t\t\t{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t\t\t\t<img src=\"{{ file_url(img_uri) }}\">
\t\t    {% endif %}
\t\t {% elseif item[\"#paragraph\"].field_mostrar_elemento_slider.0.value == \"Video\" %}
\t\t  \t{% set video_uri = file_url(item[\"#paragraph\"].field_video_elemento_slider|file_uri) %}
\t\t \t{% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t\t\t    {% if node.field_columnas_imagen.0.value == 1 %}\t
\t\t\t    \t<video src=\"{{ video_uri }}\" style=\"width: 1110px; height: 393px;\" loop muted autoplay class='video-slider'>
\t\t\t    {% elseif node.field_columnas_slider.0.value == 2 %}
\t\t\t\t\t<video src=\"{{ video_uri }}\" style=\"width: 540px; height: 322px;\" loop muted autoplay class='video-slider'>
\t\t\t    {% elseif node.field_columnas_slider.0.value == 3 %}
\t\t\t\t\t<video src=\"{{ video_uri }}\" style=\"width: 350px; height: 209px;\" loop muted autoplay class='video-slider'>
\t\t\t    {% elseif node.field_columnas_slider.0.value == 4 %}
\t\t\t\t\t<video src=\"{{ video_uri }}\" style=\"width: 255px; height: 205px;\" loop muted autoplay class='video-slider'>
\t\t\t\t{% elseif node.field_columnas_slider.0.value == \"Slider Principal\" %}
\t\t\t\t\t<video src=\"{{ video_uri }}\" style=\"width: 1110px; height: 393px;\" loop muted autoplay class='video-slider'>
\t\t\t\t{% endif %}
\t\t\t{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t\t\t\t\t<video src=\"{{ video_uri }}\" loop muted autoplay class='video-slider'>
\t\t    {% endif %}
\t\t {% else %}
\t\t Invalido
\t\t {% endif %}
\t     \t{% if has_link == true %}
     \t   \t\t</a>
     \t   \t{% endif %} 
\t     </div>
   \t    {% endfor %}
    </div>
     <a class=\"carousel-control-prev\" role=\"button\" data-slide=\"prev\" href={{ '#slider-' ~ node.id }}>
    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Previous</span>
  </a>
  <a class=\"carousel-control-next\" role=\"button\" data-slide=\"next\"  href={{ '#slider-' ~ node.id }}>
    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Next</span>
  </a>
  </div>
</article>

", "themes/custom/plantilla_buap/templates/recursos/node--slider.html.twig", "/var/www/html/devserver/dr9-plantillas-buap/web/themes/custom/plantilla_buap/templates/recursos/node--slider.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 66, "if" => 77, "block" => 116, "for" => 128, "trans" => 118);
        static $filters = array("escape" => 62, "clean_class" => 68, "merge" => 79, "raw" => 92, "first" => 128, "file_uri" => 130, "default" => 132, "image_style" => 156);
        static $functions = array("attach_library" => 62, "file_url" => 167);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block', 'for', 'trans'],
                ['escape', 'clean_class', 'merge', 'raw', 'first', 'file_uri', 'default', 'image_style'],
                ['attach_library', 'file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

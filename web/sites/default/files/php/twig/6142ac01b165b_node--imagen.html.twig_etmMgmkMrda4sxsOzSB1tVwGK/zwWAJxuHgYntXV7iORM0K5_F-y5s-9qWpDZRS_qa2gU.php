<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/plantilla_buap/templates/recursos/node--imagen.html.twig */
class __TwigTemplate_e6b916d0e406d175c0f69b9176be7dacaab6b65f6928455b5d952dd4e52be4c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'submitted' => [$this, 'block_submitted'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("bootstrap_barrio/node"), "html", null, true);
        echo "

";
        // line 65
        $context["classes"] = [0 => "node", 1 => ("node--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 67
($context["node"] ?? null), "bundle", [], "any", false, false, true, 67), 67, $this->source))), 2 => ((twig_get_attribute($this->env, $this->source,         // line 68
($context["node"] ?? null), "isPromoted", [], "method", false, false, true, 68)) ? ("node--promoted") : ("")), 3 => ((twig_get_attribute($this->env, $this->source,         // line 69
($context["node"] ?? null), "isSticky", [], "method", false, false, true, 69)) ? ("node--sticky") : ("")), 4 => (( !twig_get_attribute($this->env, $this->source,         // line 70
($context["node"] ?? null), "isPublished", [], "method", false, false, true, 70)) ? ("node--unpublished") : ("")), 5 => ((        // line 71
($context["view_mode"] ?? null)) ? (("node--view-mode-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null), 71, $this->source)))) : ("")), 6 => "clearfix"];
        // line 76
        echo "
";
        // line 77
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 77), 0, [], "any", false, false, true, 77), "value", [], "any", false, false, true, 77) == "Sencillo")) {
            // line 78
            echo "\t";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 78), 0, [], "any", false, false, true, 78), "value", [], "any", false, false, true, 78) == 1)) {
                // line 79
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 79, $this->source), [0 => "col-md-12", 1 => "imagen-1-cols"]);
                // line 80
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 80), 0, [], "any", false, false, true, 80), "value", [], "any", false, false, true, 80) == 2)) {
                // line 81
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 81, $this->source), [0 => "col-md-6", 1 => "imagen-2-cols"]);
                // line 82
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 82), 0, [], "any", false, false, true, 82), "value", [], "any", false, false, true, 82) == 3)) {
                // line 83
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 83, $this->source), [0 => "col-md-4", 1 => "imagen-3-cols"]);
                // line 84
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 84), 0, [], "any", false, false, true, 84), "value", [], "any", false, false, true, 84) == 4)) {
                // line 85
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 85, $this->source), [0 => "col-md-3", 1 => "imagen-4-cols"]);
                // line 86
                echo "\t";
            } else {
                // line 87
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 87, $this->source), [0 => "col-md-12", 1 => "imagen-1-cols"]);
                // line 88
                echo "\t";
            }
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 89
($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 89), 0, [], "any", false, false, true, 89), "value", [], "any", false, false, true, 89) == "Avanzado")) {
            // line 90
            echo "\t";
            $context["bsclasses"] = $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_personalizado_", [], "any", false, false, true, 90), 0, [], "any", false, false, true, 90), "value", [], "any", false, false, true, 90), 90, $this->source);
            echo " 
\t";
            // line 91
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 91, $this->source), [0 => ($context["bsclasses"] ?? null)]);
            // line 92
            echo "\t";
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 92, $this->source), [0 => "imagen-p-cols"]);
        }
        // line 94
        echo "
";
        // line 95
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 95), 0, [], "any", false, false, true, 95), "value", [], "any", false, false, true, 95) == "Sencillo")) {
            // line 96
            echo "\t<article";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 96), 96, $this->source), "html", null, true);
            echo ">
";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 97
($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 97), 0, [], "any", false, false, true, 97), "value", [], "any", false, false, true, 97) == "Avanzado")) {
            // line 98
            echo "\t";
            $context["height_user"] = $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_altura_columna_px_", [], "any", false, false, true, 98), 0, [], "any", false, false, true, 98), "value", [], "any", false, false, true, 98), 98, $this->source);
            // line 99
            echo "\t
\t<article";
            // line 100
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 100), 100, $this->source), "html", null, true);
            echo " style=\"height: ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["height_user"] ?? null), 100, $this->source), "html", null, true);
            echo "px; overflow: hidden; \">
";
        }
        // line 102
        echo "   <header>
    ";
        // line 103
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 103, $this->source), "html", null, true);
        echo "
    ";
        // line 104
        if ((($context["label"] ?? null) &&  !($context["page"] ?? null))) {
            // line 105
            echo "      <h2";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => "node__title"], "method", false, false, true, 105), 105, $this->source), "html", null, true);
            echo ">
        <a href=\"";
            // line 106
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 106, $this->source), "html", null, true);
            echo "\" rel=\"bookmark\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 106, $this->source), "html", null, true);
            echo "</a>
      </h2>
    ";
        }
        // line 109
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 109, $this->source), "html", null, true);
        echo "
    ";
        // line 110
        if (($context["display_submitted"] ?? null)) {
            // line 111
            echo "      <div class=\"node__meta\">
        ";
            // line 112
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["author_picture"] ?? null), 112, $this->source), "html", null, true);
            echo "
        ";
            // line 113
            $this->displayBlock('submitted', $context, $blocks);
            // line 118
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["metadata"] ?? null), 118, $this->source), "html", null, true);
            echo "
      </div>
    ";
        }
        // line 121
        echo "  </header> 
  <div";
        // line 122
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => "node__content", 1 => "clearfix"], "method", false, false, true, 122), 122, $this->source), "html", null, true);
        echo "> 
    ";
        // line 123
        $context["img_uri"] = Drupal\twig_tweak\TwigTweakExtension::fileUriFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_imagen_imagen", [], "any", false, false, true, 123), 123, $this->source));
        // line 124
        echo "\t";
        if ((( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_link_imagen", [], "any", false, false, true, 124)) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_link_imagen", [], "any", false, false, true, 124), 0, [], "any", false, false, true, 124), "url", [], "any", false, false, true, 124) != "")) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_link_imagen", [], "any", false, false, true, 124), 0, [], "any", false, false, true, 124), "url", [], "any", false, false, true, 124) != null))) {
            // line 125
            echo "\t\t";
            $context["has_link"] = true;
            // line 126
            echo "\t";
        }
        // line 127
        echo "\t";
        if ((($context["has_link"] ?? null) == true)) {
            // line 128
            echo "\t\t";
            $context["img_link"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_link_imagen", [], "any", false, false, true, 128), 0, [], "any", false, false, true, 128), "url", [], "any", false, false, true, 128);
            // line 129
            echo "\t\t";
            $context["img_link_target"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_link_imagen", [], "any", false, false, true, 129), 0, [], "any", false, false, true, 129), "options", [], "any", false, false, true, 129), "attributes", [], "any", false, false, true, 129), "target", [], "any", false, false, true, 129);
            // line 130
            echo "
\t\t";
            // line 131
            if ((($context["img_link_target"] ?? null) == "_blank")) {
                // line 132
                echo "\t\t\t";
                $context["img_link_target"] = "target='_blank'";
                // line 133
                echo "\t\t";
            } else {
                // line 134
                echo "\t\t\t";
                $context["img_link_target"] = "";
                // line 135
                echo "\t\t";
            }
            // line 136
            echo "\t\t<a href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["img_link"] ?? null), 136, $this->source), "html", null, true);
            echo "\" ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["img_link_target"] ?? null), 136, $this->source), "html", null, true);
            echo ">
\t";
        }
        // line 138
        echo "    ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 138), 0, [], "any", false, false, true, 138), "value", [], "any", false, false, true, 138) == "Sencillo")) {
            // line 139
            echo "\t    ";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 139), 0, [], "any", false, false, true, 139), "value", [], "any", false, false, true, 139) == 1)) {
                echo "\t
\t    ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 140
($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 140), 0, [], "any", false, false, true, 140), "value", [], "any", false, false, true, 140) == 2)) {
                // line 141
                echo "\t\t\t<img src=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 141, $this->source), "2_columnas_6_cols_bootstrap_"), "html", null, true);
                echo "\">
\t    ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 142
($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 142), 0, [], "any", false, false, true, 142), "value", [], "any", false, false, true, 142) == 3)) {
                // line 143
                echo "\t\t\t<img src=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 143, $this->source), "3_columnas_4_cols_bootstrap_"), "html", null, true);
                echo "\">
\t    ";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 144
($context["node"] ?? null), "field_columnas_imagen", [], "any", false, false, true, 144), 0, [], "any", false, false, true, 144), "value", [], "any", false, false, true, 144) == 4)) {
                // line 145
                echo "\t\t\t<img src=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, Drupal\twig_tweak\TwigTweakExtension::imageStyleFilter($this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 145, $this->source), "4_columnas_3_cols_bootstrap_"), "html", null, true);
                echo "\">
\t\t";
            }
            // line 147
            echo "\t";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_opciones", [], "any", false, false, true, 147), 0, [], "any", false, false, true, 147), "value", [], "any", false, false, true, 147) == "Avanzado")) {
            // line 148
            echo "\t\t<img src=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed(($context["img_uri"] ?? null), 148, $this->source)]), "html", null, true);
            echo "\">
    ";
        }
        // line 150
        echo "\t";
        if ((($context["has_link"] ?? null) == true)) {
            // line 151
            echo "   \t\t</a>
   \t";
        }
        // line 152
        echo "  
   \t</div>
</article>
";
    }

    // line 113
    public function block_submitted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 114
        echo "          <em";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["author_attributes"] ?? null), 114, $this->source), "html", null, true);
        echo ">
            ";
        // line 115
        echo t("Submitted by @author_name on @date", array("@author_name" => ($context["author_name"] ?? null), "@date" => ($context["date"] ?? null), ));
        // line 116
        echo "          </em>
        ";
    }

    public function getTemplateName()
    {
        return "themes/custom/plantilla_buap/templates/recursos/node--imagen.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 116,  287 => 115,  282 => 114,  278 => 113,  271 => 152,  267 => 151,  264 => 150,  258 => 148,  255 => 147,  249 => 145,  247 => 144,  242 => 143,  240 => 142,  235 => 141,  233 => 140,  228 => 139,  225 => 138,  217 => 136,  214 => 135,  211 => 134,  208 => 133,  205 => 132,  203 => 131,  200 => 130,  197 => 129,  194 => 128,  191 => 127,  188 => 126,  185 => 125,  182 => 124,  180 => 123,  176 => 122,  173 => 121,  166 => 118,  164 => 113,  160 => 112,  157 => 111,  155 => 110,  150 => 109,  142 => 106,  137 => 105,  135 => 104,  131 => 103,  128 => 102,  121 => 100,  118 => 99,  115 => 98,  113 => 97,  108 => 96,  106 => 95,  103 => 94,  99 => 92,  97 => 91,  92 => 90,  90 => 89,  87 => 88,  84 => 87,  81 => 86,  78 => 85,  75 => 84,  72 => 83,  69 => 82,  66 => 81,  63 => 80,  60 => 79,  57 => 78,  55 => 77,  52 => 76,  50 => 71,  49 => 70,  48 => 69,  47 => 68,  46 => 67,  45 => 65,  40 => 62,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bootstrap Barrio's theme implementation to display a node.
 *
 * Available variables:
 * - node: The node entity with limited access to object properties and methods.
     Only \"getter\" methods (method names starting with \"get\", \"has\", or \"is\")
     and a few common methods such as \"id\" and \"label\" are available. Calling
     other methods (such as node.delete) will result in an exception.
 * - label: The title of the node.
 * - content: All node items. Use {{ content }} to print them all,
 *   or print a subset such as {{ content.field_example }}. Use
 *   {{ content|without('field_example') }} to temporarily suppress the printing
 *   of a given child element.
 * - author_picture: The node author user entity, rendered using the \"compact\"
 *   view mode.
 * - metadata: Metadata for this node.
 * - date: Themed creation date field.
 * - author_name: Themed author name field.
 * - url: Direct URL of the current node.
 * - display_submitted: Whether submission information should be displayed.
 * - attributes: HTML attributes for the containing element.
 *   The attributes.class element may contain one or more of the following
 *   classes:
 *   - node: The current template type (also known as a \"theming hook\").
 *   - node--type-[type]: The current node type. For example, if the node is an
 *     \"Article\" it would result in \"node--type-article\". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node--view-mode-[view_mode]: The View Mode of the node; for example, a
 *     teaser would result in: \"node--view-mode-teaser\", and
 *     full: \"node--view-mode-full\".
 *   The following are controlled through the node publishing options.
 *   - node--promoted: Appears on nodes promoted to the front page.
 *   - node--sticky: Appears on nodes ordered above other non-sticky nodes in
 *     teaser listings.
 *   - node--unpublished: Appears on unpublished nodes visible only to site
 *     admins.
 * - title_attributes: Same as attributes, except applied to the main title
 *   tag that appears in the template.
 * - content_attributes: Same as attributes, except applied to the main
 *   content tag that appears in the template.
 * - author_attributes: Same as attributes, except applied to the author of
 *   the node tag that appears in the template.
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - view_mode: View mode; for example, \"teaser\" or \"full\".
 * - teaser: Flag for the teaser state. Will be true if view_mode is 'teaser'.
 * - page: Flag for the full page state. Will be true if view_mode is 'full'.
 * - readmore: Flag for more state. Will be true if the teaser content of the
 *   node cannot hold the main body content.
 * - logged_in: Flag for authenticated user status. Will be true when the
 *   current user is a logged-in member.
 * - is_admin: Flag for admin user status. Will be true when the current user
 *   is an administrator.
 *
 * @see template_preprocess_node()
 */
#}
{{ attach_library('bootstrap_barrio/node') }}

{%
  set classes = [
    'node',
    'node--type-' ~ node.bundle|clean_class,
    node.isPromoted() ? 'node--promoted',
    node.isSticky() ? 'node--sticky',
    not node.isPublished() ? 'node--unpublished',
    view_mode ? 'node--view-mode-' ~ view_mode|clean_class,
    'clearfix',
  ]
%}
{# Fuente: https://blog.usejournal.com/getting-drupal-8-field-values-in-twig-22b80cb609bd #}

{% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t{% if node.field_columnas_imagen.0.value == 1 %}
\t\t{% set classes = classes|merge(['col-md-12', 'imagen-1-cols']) %}
\t{% elseif node.field_columnas_imagen.0.value == 2 %}
\t\t{% set classes = classes|merge(['col-md-6', 'imagen-2-cols']) %}
\t{% elseif node.field_columnas_imagen.0.value == 3 %}
\t\t{% set classes = classes|merge(['col-md-4', 'imagen-3-cols']) %}
\t{% elseif node.field_columnas_imagen.0.value == 4 %}
\t\t{% set classes = classes|merge(['col-md-3', 'imagen-4-cols']) %}
\t{% else %}
\t\t{% set classes = classes|merge(['col-md-12', 'imagen-1-cols']) %}
\t{% endif %}
{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t{% set bsclasses = node.field_columnas_personalizado_.0.value|raw %} 
\t{% set classes = classes|merge([bsclasses]) %}
\t{% set classes = classes|merge(['imagen-p-cols']) %}
{% endif %}

{% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t<article{{ attributes.addClass(classes) }}>
{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t{% set height_user = node.field_altura_columna_px_.0.value|raw %}
\t
\t<article{{ attributes.addClass(classes) }} style=\"height: {{ height_user }}px; overflow: hidden; \">
{% endif %}
   <header>
    {{ title_prefix }}
    {% if label and not page %}
      <h2{{ title_attributes.addClass('node__title') }}>
        <a href=\"{{ url }}\" rel=\"bookmark\">{{ label }}</a>
      </h2>
    {% endif %}
    {{ title_suffix }}
    {% if display_submitted %}
      <div class=\"node__meta\">
        {{ author_picture }}
        {% block submitted %}
          <em{{ author_attributes }}>
            {% trans %}Submitted by {{ author_name }} on {{ date }}{% endtrans %}
          </em>
        {% endblock %}
        {{ metadata }}
      </div>
    {% endif %}
  </header> 
  <div{{ content_attributes.addClass('node__content', 'clearfix') }}> 
    {% set img_uri = node.field_imagen_imagen|file_uri %}
\t{% if content.field_link_imagen is not empty and node.field_link_imagen.0.url != \"\" and node.field_link_imagen.0.url != null %}
\t\t{% set has_link = true %}
\t{% endif %}
\t{% if has_link == true %}
\t\t{% set img_link = node.field_link_imagen.0.url %}
\t\t{% set img_link_target = node.field_link_imagen.0.options.attributes.target %}

\t\t{% if img_link_target == \"_blank\" %}
\t\t\t{% set img_link_target = \"target='_blank'\" %}
\t\t{% else %}
\t\t\t{% set img_link_target = \"\" %}
\t\t{% endif %}
\t\t<a href=\"{{ img_link }}\" {{ img_link_target }}>
\t{% endif %}
    {% if node.field_columnas_opciones.0.value == 'Sencillo' %}
\t    {% if node.field_columnas_imagen.0.value == 1 %}\t
\t    {% elseif node.field_columnas_imagen.0.value == 2 %}
\t\t\t<img src=\"{{ img_uri | image_style('2_columnas_6_cols_bootstrap_') }}\">
\t    {% elseif node.field_columnas_imagen.0.value == 3 %}
\t\t\t<img src=\"{{ img_uri | image_style('3_columnas_4_cols_bootstrap_') }}\">
\t    {% elseif node.field_columnas_imagen.0.value == 4 %}
\t\t\t<img src=\"{{ img_uri | image_style('4_columnas_3_cols_bootstrap_') }}\">
\t\t{% endif %}
\t{% elseif node.field_columnas_opciones.0.value == 'Avanzado' %}
\t\t<img src=\"{{ file_url(img_uri) }}\">
    {% endif %}
\t{% if has_link == true %}
   \t\t</a>
   \t{% endif %}  
   \t</div>
</article>
", "themes/custom/plantilla_buap/templates/recursos/node--imagen.html.twig", "/var/www/html/devserver/dr9-plantillas-buap/web/themes/custom/plantilla_buap/templates/recursos/node--imagen.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 65, "if" => 77, "block" => 113, "trans" => 115);
        static $filters = array("escape" => 62, "clean_class" => 67, "merge" => 79, "raw" => 90, "file_uri" => 123, "image_style" => 141);
        static $functions = array("attach_library" => 62, "file_url" => 148);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block', 'trans'],
                ['escape', 'clean_class', 'merge', 'raw', 'file_uri', 'image_style'],
                ['attach_library', 'file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/plantilla_buap/templates/recursos/node--menu-recurso.html.twig */
class __TwigTemplate_f0844e8daa48cd908e849cca2bf1b1466b8cfe6c1cd99482f145cd033e794db8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'submitted' => [$this, 'block_submitted'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->attachLibrary("bootstrap_barrio/node"), "html", null, true);
        echo "

";
        // line 65
        $context["classes"] = [0 => "node", 1 => ("node--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 67
($context["node"] ?? null), "bundle", [], "any", false, false, true, 67), 67, $this->source))), 2 => ((twig_get_attribute($this->env, $this->source,         // line 68
($context["node"] ?? null), "isPromoted", [], "method", false, false, true, 68)) ? ("node--promoted") : ("")), 3 => ((twig_get_attribute($this->env, $this->source,         // line 69
($context["node"] ?? null), "isSticky", [], "method", false, false, true, 69)) ? ("node--sticky") : ("")), 4 => (( !twig_get_attribute($this->env, $this->source,         // line 70
($context["node"] ?? null), "isPublished", [], "method", false, false, true, 70)) ? ("node--unpublished") : ("")), 5 => ((        // line 71
($context["view_mode"] ?? null)) ? (("node--view-mode-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null), 71, $this->source)))) : ("")), 6 => "clearfix"];
        // line 76
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_diseno_menu_recurso", [], "any", false, false, true, 76), 0, [], "any", false, false, true, 76), "value", [], "any", false, false, true, 76) != "Personalizado")) {
            echo " 
\t";
            // line 77
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_diseno_menu_recurso", [], "any", false, false, true, 77), 0, [], "any", false, false, true, 77), "value", [], "any", false, false, true, 77) == "Hover")) {
                // line 78
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 78, $this->source), [0 => "menu-hover"]);
                // line 79
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_diseno_menu_recurso", [], "any", false, false, true, 79), 0, [], "any", false, false, true, 79), "value", [], "any", false, false, true, 79) == "Caret")) {
                // line 80
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 80, $this->source), [0 => "menu-caret"]);
                // line 81
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_diseno_menu_recurso", [], "any", false, false, true, 81), 0, [], "any", false, false, true, 81), "value", [], "any", false, false, true, 81) == "Azul")) {
                // line 82
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 82, $this->source), [0 => "menu-azul"]);
                // line 83
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_diseno_menu_recurso", [], "any", false, false, true, 83), 0, [], "any", false, false, true, 83), "value", [], "any", false, false, true, 83) == "Blanco")) {
                // line 84
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 84, $this->source), [0 => "menu-blanco"]);
                // line 85
                echo "\t";
            }
            // line 86
            echo "\t
\t";
            // line 87
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_menu_recurso", [], "any", false, false, true, 87), 0, [], "any", false, false, true, 87), "value", [], "any", false, false, true, 87) == 2)) {
                // line 88
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 88, $this->source), [0 => "col-md-6", 1 => "menu-2-cols"]);
                // line 89
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_menu_recurso", [], "any", false, false, true, 89), 0, [], "any", false, false, true, 89), "value", [], "any", false, false, true, 89) == 3)) {
                // line 90
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 90, $this->source), [0 => "col-md-4", 1 => "menu-3-cols"]);
                // line 91
                echo "\t";
            } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_menu_recurso", [], "any", false, false, true, 91), 0, [], "any", false, false, true, 91), "value", [], "any", false, false, true, 91) == 4)) {
                // line 92
                echo "\t\t";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 92, $this->source), [0 => "col-md-3", 1 => "menu-4-cols"]);
                // line 93
                echo "\t";
            } else {
                // line 94
                echo "\t
\t";
            }
        } else {
            // line 97
            echo "\t";
            $context["bsclasses"] = $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_columnas_personalizado_", [], "any", false, false, true, 97), 0, [], "any", false, false, true, 97), "value", [], "any", false, false, true, 97), 97, $this->source);
            echo " 
\t";
            // line 98
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 98, $this->source), [0 => ($context["bsclasses"] ?? null)]);
            // line 99
            echo "\t";
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null), 99, $this->source), [0 => "menu-p-cols"]);
        }
        // line 101
        echo "\t<article";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 101), 101, $this->source), "html", null, true);
        echo ">

   <header>
    ";
        // line 104
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null), 104, $this->source), "html", null, true);
        echo "
    ";
        // line 105
        if ((($context["label"] ?? null) &&  !($context["page"] ?? null))) {
            // line 106
            echo "      <h2";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["title_attributes"] ?? null), "addClass", [0 => "node__title"], "method", false, false, true, 106), 106, $this->source), "html", null, true);
            echo ">
        <a href=\"";
            // line 107
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 107, $this->source), "html", null, true);
            echo "\" rel=\"bookmark\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 107, $this->source), "html", null, true);
            echo "</a>
      </h2>
    ";
        }
        // line 110
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null), 110, $this->source), "html", null, true);
        echo "
    ";
        // line 111
        if (($context["display_submitted"] ?? null)) {
            // line 112
            echo "      <div class=\"node__meta\">
        ";
            // line 113
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["author_picture"] ?? null), 113, $this->source), "html", null, true);
            echo "
        ";
            // line 114
            $this->displayBlock('submitted', $context, $blocks);
            // line 119
            echo "        ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["metadata"] ?? null), 119, $this->source), "html", null, true);
            echo "
      </div>
    ";
        }
        // line 122
        echo "  </header> 
  <div";
        // line 123
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [0 => "node__content", 1 => "clearfix"], "method", false, false, true, 123), 123, $this->source), "html", null, true);
        echo "> 
\t<div style=\"position: relative\">
\t\t";
        // line 125
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 125, $this->source), "html", null, true);
        echo "
\t\t";
        // line 126
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_imagen_fondo_menu_recurso", [], "any", false, false, true, 126), 126, $this->source), "html", null, true);
        echo "
\t\t";
        // line 127
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_links_menu_recurso", [], "any", false, false, true, 127), 127, $this->source), "html", null, true);
        echo "
\t</div>
\t
\t 
   \t</div>
</article>
";
    }

    // line 114
    public function block_submitted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 115
        echo "          <em";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["author_attributes"] ?? null), 115, $this->source), "html", null, true);
        echo ">
            ";
        // line 116
        echo t("Submitted by @author_name on @date", array("@author_name" => ($context["author_name"] ?? null), "@date" => ($context["date"] ?? null), ));
        // line 117
        echo "          </em>
        ";
    }

    public function getTemplateName()
    {
        return "themes/custom/plantilla_buap/templates/recursos/node--menu-recurso.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 117,  206 => 116,  201 => 115,  197 => 114,  186 => 127,  182 => 126,  178 => 125,  173 => 123,  170 => 122,  163 => 119,  161 => 114,  157 => 113,  154 => 112,  152 => 111,  147 => 110,  139 => 107,  134 => 106,  132 => 105,  128 => 104,  121 => 101,  117 => 99,  115 => 98,  110 => 97,  105 => 94,  102 => 93,  99 => 92,  96 => 91,  93 => 90,  90 => 89,  87 => 88,  85 => 87,  82 => 86,  79 => 85,  76 => 84,  73 => 83,  70 => 82,  67 => 81,  64 => 80,  61 => 79,  58 => 78,  56 => 77,  52 => 76,  50 => 71,  49 => 70,  48 => 69,  47 => 68,  46 => 67,  45 => 65,  40 => 62,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bootstrap Barrio's theme implementation to display a node.
 *
 * Available variables:
 * - node: The node entity with limited access to object properties and methods.
     Only \"getter\" methods (method names starting with \"get\", \"has\", or \"is\")
     and a few common methods such as \"id\" and \"label\" are available. Calling
     other methods (such as node.delete) will result in an exception.
 * - label: The title of the node.
 * - content: All node items. Use {{ content }} to print them all,
 *   or print a subset such as {{ content.field_example }}. Use
 *   {{ content|without('field_example') }} to temporarily suppress the printing
 *   of a given child element.
 * - author_picture: The node author user entity, rendered using the \"compact\"
 *   view mode.
 * - metadata: Metadata for this node.
 * - date: Themed creation date field.
 * - author_name: Themed author name field.
 * - url: Direct URL of the current node.
 * - display_submitted: Whether submission information should be displayed.
 * - attributes: HTML attributes for the containing element.
 *   The attributes.class element may contain one or more of the following
 *   classes:
 *   - node: The current template type (also known as a \"theming hook\").
 *   - node--type-[type]: The current node type. For example, if the node is an
 *     \"Article\" it would result in \"node--type-article\". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node--view-mode-[view_mode]: The View Mode of the node; for example, a
 *     teaser would result in: \"node--view-mode-teaser\", and
 *     full: \"node--view-mode-full\".
 *   The following are controlled through the node publishing options.
 *   - node--promoted: Appears on nodes promoted to the front page.
 *   - node--sticky: Appears on nodes ordered above other non-sticky nodes in
 *     teaser listings.
 *   - node--unpublished: Appears on unpublished nodes visible only to site
 *     admins.
 * - title_attributes: Same as attributes, except applied to the main title
 *   tag that appears in the template.
 * - content_attributes: Same as attributes, except applied to the main
 *   content tag that appears in the template.
 * - author_attributes: Same as attributes, except applied to the author of
 *   the node tag that appears in the template.
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 * - view_mode: View mode; for example, \"teaser\" or \"full\".
 * - teaser: Flag for the teaser state. Will be true if view_mode is 'teaser'.
 * - page: Flag for the full page state. Will be true if view_mode is 'full'.
 * - readmore: Flag for more state. Will be true if the teaser content of the
 *   node cannot hold the main body content.
 * - logged_in: Flag for authenticated user status. Will be true when the
 *   current user is a logged-in member.
 * - is_admin: Flag for admin user status. Will be true when the current user
 *   is an administrator.
 *
 * @see template_preprocess_node()
 */
#}
{{ attach_library('bootstrap_barrio/node') }}

{%
  set classes = [
    'node',
    'node--type-' ~ node.bundle|clean_class,
    node.isPromoted() ? 'node--promoted',
    node.isSticky() ? 'node--sticky',
    not node.isPublished() ? 'node--unpublished',
    view_mode ? 'node--view-mode-' ~ view_mode|clean_class,
    'clearfix',
  ]
%}
{# Fuente: https://blog.usejournal.com/getting-drupal-8-field-values-in-twig-22b80cb609bd #}
{% if node.field_diseno_menu_recurso.0.value != 'Personalizado' %} 
\t{% if node.field_diseno_menu_recurso.0.value == 'Hover' %}
\t\t{% set classes = classes|merge(['menu-hover']) %}
\t{% elseif node.field_diseno_menu_recurso.0.value == 'Caret' %}
\t\t{% set classes = classes|merge(['menu-caret']) %}
\t{% elseif node.field_diseno_menu_recurso.0.value == 'Azul' %}
\t\t{% set classes = classes|merge(['menu-azul']) %}
\t{% elseif node.field_diseno_menu_recurso.0.value == 'Blanco' %}
\t\t{% set classes = classes|merge(['menu-blanco']) %}
\t{% endif %}
\t
\t{% if node.field_columnas_menu_recurso.0.value == 2 %}
\t\t{% set classes = classes|merge(['col-md-6', 'menu-2-cols']) %}
\t{% elseif node.field_columnas_menu_recurso.0.value == 3 %}
\t\t{% set classes = classes|merge(['col-md-4', 'menu-3-cols']) %}
\t{% elseif node.field_columnas_menu_recurso.0.value == 4 %}
\t\t{% set classes = classes|merge(['col-md-3', 'menu-4-cols']) %}
\t{% else %}
\t
\t{% endif %}
{% else %}
\t{% set bsclasses = node.field_columnas_personalizado_.0.value|raw %} 
\t{% set classes = classes|merge([bsclasses]) %}
\t{% set classes = classes|merge(['menu-p-cols']) %}
{% endif %}
\t<article{{ attributes.addClass(classes) }}>

   <header>
    {{ title_prefix }}
    {% if label and not page %}
      <h2{{ title_attributes.addClass('node__title') }}>
        <a href=\"{{ url }}\" rel=\"bookmark\">{{ label }}</a>
      </h2>
    {% endif %}
    {{ title_suffix }}
    {% if display_submitted %}
      <div class=\"node__meta\">
        {{ author_picture }}
        {% block submitted %}
          <em{{ author_attributes }}>
            {% trans %}Submitted by {{ author_name }} on {{ date }}{% endtrans %}
          </em>
        {% endblock %}
        {{ metadata }}
      </div>
    {% endif %}
  </header> 
  <div{{ content_attributes.addClass('node__content', 'clearfix') }}> 
\t<div style=\"position: relative\">
\t\t{{ label }}
\t\t{{ content.field_imagen_fondo_menu_recurso }}
\t\t{{ content.field_links_menu_recurso }}
\t</div>
\t
\t 
   \t</div>
</article>
", "themes/custom/plantilla_buap/templates/recursos/node--menu-recurso.html.twig", "/var/www/html/devserver/dr9-plantillas-buap/web/themes/custom/plantilla_buap/templates/recursos/node--menu-recurso.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 65, "if" => 76, "block" => 114, "trans" => 116);
        static $filters = array("escape" => 62, "clean_class" => 67, "merge" => 78, "raw" => 97);
        static $functions = array("attach_library" => 62);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block', 'trans'],
                ['escape', 'clean_class', 'merge', 'raw'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
